package ro.johann.controller;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import ro.johann.dto.NGramInputDto;
import ro.johann.service.ngram.INGramService;

import java.security.InvalidParameterException;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * Created by Johann on 5/26/2017.
 */
public class NGramControllerTest {

    @InjectMocks
    NGramController nGramController;

    @Mock
    INGramService nGramServiceMock;

    MockMvc mockMvc;

    @Before
    public void init() {

        MockitoAnnotations.initMocks(this);
        mockMvc = standaloneSetup(nGramController).build();
    }

    @Test
    public void countNGrams_whenCalledByUrl_shouldMapOnCountNGramsMethod() throws Exception {

        JSONObject json = new JSONObject();
        json.put("maxNGramCount", 3);
        json.put("text", "do you know that I know you know that I know that ?");

        mockMvc.perform(post("/ngram")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json.toString()))
                .andExpect(status().isOk());
    }

    @Test
    public void countNGrams_whenNotReceivingAResponseBody_shouldRespondWithBadRequest() throws Exception {

        mockMvc.perform(post("/ngram"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void countNGrams_whenNotReceivingATextJsonKeyInBody_shouldRespondWithBadReqeust() throws Exception {

        JSONObject json = new JSONObject();
        json.put("maxNGramCount", 3);
        json.put("something-else", "do you know that I know you know that I know that ?");

        mockMvc.perform(post("/ngram")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json.toString()))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void countNGrams_whenNotReceivingAMaxNGramCountJsonKeyInBody_shouldRespondWithBadReqeust() throws Exception {

        JSONObject json = new JSONObject();
        json.put("something-else", 3);
        json.put("text", "do you know that I know you know that I know that ?");

        mockMvc.perform(post("/ngram")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json.toString()))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void countNGrams_whenReceivingAnEmptyTextInJson_shouldRespondWithBadRequest() throws Exception {

        JSONObject json = new JSONObject();
        json.put("maxNGramCount", 3);
        json.put("text", "");

        mockMvc.perform(post("/ngram")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json.toString()))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void countNGrams_whenReceivingAnInvalidMaxNGramCountInJson_shouldRespondWithBadRequest() throws Exception {

        JSONObject json = new JSONObject();
        json.put("maxNGramCount", 0);
        json.put("text", "do you know that I know you know that I know that ?");

        mockMvc.perform(post("/ngram")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json.toString()))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void countNGrams_whenCalled_shouldCallTheNGramService() throws Exception {

        JSONObject json = new JSONObject();
        json.put("maxNGramCount", 3);
        json.put("text", "do you know that I know you know that I know that ?");

        mockMvc.perform(post("/ngram")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json.toString()))
                .andExpect(status().isOk());
        verify(nGramServiceMock, times(1)).countNGrams(anyObject());
    }

    @Test(expected = InvalidParameterException.class)
    public void countNGrams_whenNGramServiceThrowsException_shouldThrowItAlso() throws Exception {

        NGramInputDto input = new NGramInputDto(2, "aaa bbb aaa");
        doThrow(new InvalidParameterException()).when(nGramServiceMock).countNGrams(anyObject());

        nGramController.countNGrams(input);
    }
}
