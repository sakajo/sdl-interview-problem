package ro.johann.controller;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import ro.johann.dto.TextDto;
import ro.johann.service.format.IFormatService;

import java.security.InvalidParameterException;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * Created by Johann on 5/26/2017.
 */
public class FormatControllerTest {

    @InjectMocks
    FormatController formatController;

    @Mock
    IFormatService formatServiceMock;

    MockMvc mockMvc;

    @Before
    public void init() {

        MockitoAnnotations.initMocks(this);
        mockMvc = standaloneSetup(formatController).build();
    }

    @Test
    public void format_whenCalledByUrl_shouldMapOnFormatMethod() throws Exception {

        JSONObject json = new JSONObject();
        json.put("text", "PP 2 2 l' homme $ the man & blah");

        mockMvc.perform(post("/format")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json.toString()))
                .andExpect(status().isOk());
    }

    @Test
    public void format_whenNotReceivingAResponseBody_shouldRespondWithBadRequest() throws Exception {

        mockMvc.perform(post("/format"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void format_whenNotReceivingATextJsonKey_shouldRespondWithBadRequest() throws Exception {

        JSONObject json = new JSONObject();
        json.put("something-else", "PP 2 2 l' homme $ the man & blah");

        mockMvc.perform(post("/format")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json.toString()))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void format_whenReceivingAnEmptyTextJsonKey_shouldRespondWithBadRequest() throws Exception {

        JSONObject json = new JSONObject();
        json.put("text", "");

        mockMvc.perform(post("/format")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json.toString()))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void format_whenCalled_shouldCallTheFormatService() throws Exception {

        JSONObject json = new JSONObject();
        json.put("text", "PP 2 2 l' homme $ the man & blah");

        mockMvc.perform(post("/format")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json.toString()))
                .andExpect(status().isOk());
        verify(formatServiceMock, times(1)).format(anyObject());
    }

    @Test(expected = InvalidParameterException.class)
    public void format_whenFormatServiceThrowsException_shouldThrowItAlso() throws Exception {

        doThrow(new InvalidParameterException()).when(formatServiceMock).format(anyObject());

        formatController.format(new TextDto(anyString()));
    }
}
