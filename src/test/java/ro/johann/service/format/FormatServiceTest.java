package ro.johann.service.format;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import ro.johann.dto.TextDto;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

/**
 * Created by Johann on 5/26/2017.
 */
public class FormatServiceTest {

    FormatService formatService;

    @Mock
    IPhrasePairsConverter phrasePairsConverterMock;

    @Mock
    IPhrasePairsFormatter phrasePairsFormatterMock;

    @Before
    public void init() {
        formatService = new FormatService();
        MockitoAnnotations.initMocks(this);
        formatService.setPhrasePairsConverter(phrasePairsConverterMock);
        formatService.setPhrasePairsFormatter(phrasePairsFormatterMock);
    }

    @Test
    public void format_whenCalled_shouldCallPhrasePairsConverterWithTheRightArgument() {

        String text = "abc";
        TextDto input = new TextDto(text);

        formatService.format(input);

        verify(phrasePairsConverterMock, times(1)).convert(text);
    }

    @Test
    public void format_whenCalled_shouldCallFormatterWithTheReturnValueOfTheConverter() {

        TextDto input = new TextDto("abc");
        List<PhrasePair> converterReturnValue = new ArrayList<>();
        converterReturnValue.add(new PhrasePair(Arrays.asList("aa", "bb"), Arrays.asList("cc", "dd"), "xyz"));
        when(phrasePairsConverterMock.convert(anyString())).thenReturn(converterReturnValue);

        formatService.format(input);

        verify(phrasePairsFormatterMock, times(1)).format(converterReturnValue);
    }

    @Test
    public void format_whenCalled_shouldReturnTheValueGotFromTheFormatter() {

        String expectedString = "expected";
        TextDto expectedOutcome = new TextDto(expectedString);
        when(phrasePairsFormatterMock.format(anyObject())).thenReturn(expectedString);

        TextDto actualOutcome = formatService.format(new TextDto(anyString()));

        Assert.assertEquals(expectedOutcome.getText(), actualOutcome.getText());
    }

    @Test(expected = InvalidParameterException.class)
    public void format_whenConverterThrowsInvalidParameterException_shouldThrowItAlso() {

        doThrow(new InvalidParameterException()).when(phrasePairsConverterMock).convert(anyString());

        formatService.format(new TextDto(anyString()));
    }

    @Test(expected = InvalidParameterException.class)
    public void format_whenFormatterThrowsInvalidParameterException_shouldThrowItAlso() {

        doThrow(new InvalidParameterException()).when(phrasePairsFormatterMock).format(anyObject());

        formatService.format(new TextDto(anyString()));
    }

}
