package ro.johann.service.format;

import org.junit.Assert;
import org.junit.Test;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Johann on 5/25/2017.
 */
public class PhrasePairsConverterTest {

    PhrasePairsConverter phrasePairsConverter = new PhrasePairsConverter();

    @Test
    public void convert_whenGivenAValidSinglePhrasePairText_shouldReturnTheCorrectPhrasePairList() {

        String input = "PP 2 2 l' homme $ the man & blah";
        PhrasePair phrasePair = new PhrasePair(Arrays.asList("l'", "homme"), Arrays.asList("the", "man"), "blah");
        List<PhrasePair> expected = Arrays.asList(phrasePair);

        List<PhrasePair> actual = phrasePairsConverter.convert(input);

        for (int index = 0; index < expected.size(); index++) {
            Assert.assertEquals(expected.get(index).getSourceTokens(), actual.get(index).getSourceTokens());
            Assert.assertEquals(expected.get(index).getTargetTokens(), actual.get(index).getTargetTokens());
            Assert.assertEquals(expected.get(index).getOtherInfo(), actual.get(index).getOtherInfo());
        }
    }

    @Test
    public void convert_whenGivenAValidMultiplePhrasePairText_shouldReturnTheCorrectPhrasePairList() {

        String input = "PP 2 2 l' homme $ the man & blah PP 2 2 the woman $ la femme & lorem";
        List<PhrasePair> expected = new ArrayList<>();
        expected.add(new PhrasePair(Arrays.asList("l'", "homme"), Arrays.asList("the", "man"), "blah"));
        expected.add(new PhrasePair(Arrays.asList("the", "woman"), Arrays.asList("la", "femme"), "lorem"));

        List<PhrasePair> actual = phrasePairsConverter.convert(input);

        for (int index = 0; index < expected.size(); index++) {
            Assert.assertEquals(expected.get(index).getSourceTokens(), actual.get(index).getSourceTokens());
            Assert.assertEquals(expected.get(index).getTargetTokens(), actual.get(index).getTargetTokens());
            Assert.assertEquals(expected.get(index).getOtherInfo(), actual.get(index).getOtherInfo());
        }
    }

    @Test(expected = InvalidParameterException.class)
    public void convert_whenGivenAnInvalidPrefix_shouldThrowInvalidParameterException() {

        String input = "QR 2 2 l' homme $ the man & blah";

        phrasePairsConverter.convert(input);
    }

    @Test(expected = InvalidParameterException.class)
    public void convert_whenGivenAnIncorrectNumberOfSourceTokens_shouldThrowInvalidParameterException() {

        String input = "PP 2 2 l' homme aaa bbb $ the man & blah";

        phrasePairsConverter.convert(input);
    }

    @Test(expected = InvalidParameterException.class)
    public void convert_whenGivenAnIncorrectNumberOfTargetTokens_shouldThrowInvalidParameterException() {

        String input = "PP 2 3 l' homme $ the man & blah";

        phrasePairsConverter.convert(input);
    }

    @Test(expected = InvalidParameterException.class)
    public void convert_whenGivenAWrongTokensSeparator_shouldThrowInvalidParameterException() {

        String input = "PP 2 2 l' homme X the man & blah";

        phrasePairsConverter.convert(input);
    }

    @Test(expected = InvalidParameterException.class)
    public void convert_whenGivenAWrongOtherInfoSeparator_shouldThrowInvalidParameterException() {

        String input = "PP 2 2 l' homme $ the man * blah";

        phrasePairsConverter.convert(input);
    }

    @Test(expected = InvalidParameterException.class)
    public void convert_whenGivenNoOtherInfo_shouldThrowInvalidParameterException() {

        String input = "PP 2 2 l' homme $ the man";

        phrasePairsConverter.convert(input);
    }

    @Test(expected = InvalidParameterException.class)
    public void convert_whenGivenIncompletePhrasePair_shouldThrowInvalidParameterException() {

        String input = "PP 2 2 l' homme $ the man & blah PP 4";

        phrasePairsConverter.convert(input);
    }
}
