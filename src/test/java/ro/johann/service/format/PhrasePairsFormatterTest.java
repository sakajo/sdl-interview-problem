package ro.johann.service.format;

import org.junit.Test;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Johann on 5/25/2017.
 */
public class PhrasePairsFormatterTest {

    PhrasePairsFormatter phrasePairsFormatter = new PhrasePairsFormatter();

    @Test
    public void format_whenGivenAValidListOfPhrasePairs_shouldReturnACorrectString() {

        List<List<PhrasePair>> inputValues = new ArrayList<>();
        PhrasePair phrasePair1 = new PhrasePair(Arrays.asList("l'", "homme"), Arrays.asList("the", "man"), "blah");
        PhrasePair phrasePair2 = new PhrasePair(Arrays.asList("the", "woman"), Arrays.asList("la", "femme"), "lorem");
        inputValues.add(Arrays.asList(phrasePair1));
        inputValues.add(Arrays.asList(phrasePair1, phrasePair2));
        List<String> exptectedResult = new ArrayList<>();
        exptectedResult.add("2 l' homme $ 2 the man & blah |");
        exptectedResult.add("2 l' homme $ 2 the man & blah | 2 the woman $ 2 la femme & lorem |");
        List<String> actualResult = new ArrayList<>();

        for (List<PhrasePair> input : inputValues)
            actualResult.add(phrasePairsFormatter.format(input));
    }

    @Test(expected = InvalidParameterException.class)
    public void format_whenGivenANullArgument_shouldThrowInvalidParameterException() {

        List<PhrasePair> input = null;

        phrasePairsFormatter.format(input);
    }

    @Test(expected = InvalidParameterException.class)
    public void format_whenGivenAnEmptyListAsArgument_shouldThrowInvalidParameterException() {

        List<PhrasePair> input = new ArrayList<PhrasePair>();

        phrasePairsFormatter.format(input);
    }
}
