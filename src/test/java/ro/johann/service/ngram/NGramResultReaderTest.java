package ro.johann.service.ngram;

import org.junit.Assert;
import org.junit.Test;
import ro.johann.dto.NGramResultDto;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Johann on 5/26/2017.
 */
public class NGramResultReaderTest {

    private NGramResultReader resultReader = new NGramResultReader();

    @Test
    public void readResult_whenGivenANGramNode_shouldReturnTheCorrectSortedList() {

        NGramNode input = new NGramNode();
        input.addChild("aaa");
        input.getChildren().get("aaa").incrementScore();
        input.getChildren().get("aaa").incrementScore();
        input.getChildren().get("aaa").addChild("bbb");
        input.getChildren().get("aaa").getChildren().get("bbb").incrementScore();
        input.addChild("bbb");
        input.getChildren().get("bbb").incrementScore();
        input.getChildren().get("bbb").addChild("aaa");
        input.getChildren().get("bbb").getChildren().get("aaa").incrementScore();

        List<NGramResultDto> expectedOutcome = new ArrayList<>();
        expectedOutcome.add(new NGramResultDto("aaa bbb", 2, 1));
        expectedOutcome.add(new NGramResultDto("bbb aaa", 2, 1));
        expectedOutcome.add(new NGramResultDto("aaa", 1, 2));
        expectedOutcome.add(new NGramResultDto("bbb", 1, 1));

        List<NGramResultDto> actualResult = resultReader.readResult(input);

        Assert.assertEquals(actualResult.size(), expectedOutcome.size());
        for (int index = 0; index < expectedOutcome.size(); index++) {
            Assert.assertEquals(expectedOutcome.get(index).getNGram(), actualResult.get(index).getNGram());
            Assert.assertEquals(expectedOutcome.get(index).getCount(), actualResult.get(index).getCount());
        }
    }

    @Test(expected = InvalidParameterException.class)
    public void readResult_whenGivenNullNGramNode_shouldThrowInvalidParameterException() {

        NGramNode input = null;

        resultReader.readResult(input);
    }
}
