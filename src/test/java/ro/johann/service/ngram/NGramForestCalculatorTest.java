package ro.johann.service.ngram;

import org.junit.Assert;
import org.junit.Test;

import java.security.InvalidParameterException;

/**
 * Created by Johann on 5/26/2017.
 */
public class NGramForestCalculatorTest {

    private NGramForestCalculator forestCalculator = new NGramForestCalculator();

    @Test
    public void calculate_whenGivenAText_shouldCreateANGramForest() {

        String inputText = "aaa bbb aaa";
        int inputMaxNGrams = 2;

        NGramNode outcome = forestCalculator.calculate(inputText, inputMaxNGrams);

        Assert.assertEquals(2, outcome.getChildren().size());
        Assert.assertEquals(2, outcome.getChildren().get("aaa").getScore());
        Assert.assertEquals(1, outcome.getChildren().get("bbb").getScore());
        Assert.assertEquals(1, outcome.getChildren().get("aaa").getChildren().size());
        Assert.assertEquals(1, outcome.getChildren().get("bbb").getChildren().size());
        Assert.assertEquals(1, outcome.getChildren().get("aaa").getChildren().get("bbb").getScore());
        Assert.assertEquals(1, outcome.getChildren().get("bbb").getChildren().get("aaa").getScore());
    }

    @Test(expected = InvalidParameterException.class)
    public void calculate_whenGivenNullInputText_shouldThrowInvalidParameterException() {

        String inputText = null;
        int inputMaxNGrams = 3;

        forestCalculator.calculate(inputText, inputMaxNGrams);
    }

    @Test(expected = InvalidParameterException.class)
    public void calculate_whenGivenEmptyInputString_shouldThrowInvalidParameterException() {

        String inputText = "";
        int inputMaxNGrams = 10;

        forestCalculator.calculate(inputText, inputMaxNGrams);
    }

    @Test(expected = InvalidParameterException.class)
    public void calculate_whenGivenInvalidMaxNGrams_shouldThrowInvalidParameterException() {

        String inputText = "aaa bbb ccc";
        int inputMaxNGrams = 0;

        forestCalculator.calculate(inputText, inputMaxNGrams);

    }

}
