package ro.johann.service.ngram;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import ro.johann.dto.NGramInputDto;
import ro.johann.dto.NGramResultDto;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

/**
 * Created by Johann on 5/26/2017.
 */
public class NGramServiceTest {

    NGramService nGramService;

    @Mock
    INGramForestCalculator forestCalculatorMock;

    @Mock
    INGramResultReader resultReaderMock;

    @Before
    public void init() {
        nGramService = new NGramService();
        MockitoAnnotations.initMocks(this);
        nGramService.setForestCalculator(forestCalculatorMock);
        nGramService.setResultReader(resultReaderMock);
    }

    @Test
    public void countNGrams_whenCalled_shouldCallForestCalculatorWithTheRightArguments() {

        String inputText = "aaa bbb aaa";
        int maxNGramCount = 2;
        NGramInputDto input = new NGramInputDto(maxNGramCount, inputText);

        nGramService.countNGrams(input);

        verify(forestCalculatorMock, times(1)).calculate(inputText, maxNGramCount);
    }

    @Test
    public void countNGrams_whenCalled_shouldCallResultReaderWithTheArgumentGotFromTheForestCalculator() {

        NGramNode forestCalculatorReturn = new NGramNode();
        when(forestCalculatorMock.calculate(anyString(), anyInt())).thenReturn(forestCalculatorReturn);
        NGramInputDto input = new NGramInputDto(2, "aaa bbb aaa");

        nGramService.countNGrams(input);

        verify(resultReaderMock, times(1)).readResult(forestCalculatorReturn);
    }

    @Test
    public void countNGrams_whenCalled_shouldReturnTheValueGotFromTheResultReader() {

        List<NGramResultDto> expectedResponse = new ArrayList<>();
        expectedResponse.add(new NGramResultDto("aaa", 1, 2));
        expectedResponse.add(new NGramResultDto("bbb", 1, 1));
        when(resultReaderMock.readResult(anyObject())).thenReturn(expectedResponse);
        NGramInputDto input = new NGramInputDto(2, "aaa bbb aaa");

        List<NGramResultDto> actualResponse = nGramService.countNGrams(input);

        Assert.assertEquals(expectedResponse.size(), actualResponse.size());
        for (int index = 0; index < actualResponse.size(); index++) {
            Assert.assertEquals(expectedResponse.get(index).getNGram(), actualResponse.get(index).getNGram());
            Assert.assertEquals(expectedResponse.get(index).getCount(), actualResponse.get(index).getCount());
            Assert.assertEquals(expectedResponse.get(index).getLength(), actualResponse.get(index).getLength());
        }
    }

    @Test(expected = InvalidParameterException.class)
    public void countNGrams_whenForestCalculatorThrowsInvalidParameterException_shouldThrowItAlso() {

        doThrow(new InvalidParameterException()).when(forestCalculatorMock).calculate(anyString(), anyInt());
        NGramInputDto input = new NGramInputDto(2, "aaa bbb aaa");

        nGramService.countNGrams(input);
    }

    @Test(expected = InvalidParameterException.class)
    public void countNGrams_whenResultReaderThrowsInvalidParameterException_shouldThrowItAlso() {

        doThrow(new InvalidParameterException()).when(resultReaderMock).readResult(anyObject());
        NGramInputDto input = new NGramInputDto(2, "aaa bbb aaa");

        nGramService.countNGrams(input);
    }
}
