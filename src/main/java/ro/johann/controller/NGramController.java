package ro.johann.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ro.johann.dto.NGramInputDto;
import ro.johann.dto.NGramResultDto;
import ro.johann.service.ngram.INGramService;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Johann on 5/25/2017.
 */
@RestController
public class NGramController {

    @Autowired
    private INGramService nGramService;

    @RequestMapping(value = "/ngram", method = RequestMethod.POST)
    public ResponseEntity<List<NGramResultDto>> countNGrams(@Valid @RequestBody NGramInputDto nGramInput) {

        System.out.println("here I am");
        List<NGramResultDto> nGramResults = nGramService.countNGrams(nGramInput);
        return new ResponseEntity<>(nGramResults, HttpStatus.OK);
    }
}
