package ro.johann.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ro.johann.dto.TextDto;
import ro.johann.service.format.IFormatService;

import javax.validation.Valid;

/**
 * Created by Johann on 5/25/2017.
 */
@RestController
public class FormatController {

    @Autowired
    private IFormatService formatService;

    @RequestMapping(value = "/format", method = RequestMethod.POST)
    public ResponseEntity format(@Valid @RequestBody TextDto rawText) {

        TextDto formattedText = formatService.format(rawText);
        return new ResponseEntity(formattedText, HttpStatus.OK);
    }
}
