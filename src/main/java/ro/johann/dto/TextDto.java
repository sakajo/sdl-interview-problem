package ro.johann.dto;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

/**
 * Created by Johann on 5/25/2017.
 */
public class TextDto {

    @NotNull
    @NotEmpty
    private String text;

    public TextDto() { }

    public TextDto(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
