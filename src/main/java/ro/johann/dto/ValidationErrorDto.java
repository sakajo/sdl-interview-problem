package ro.johann.dto;

/**
 * Created by Johann on 5/25/2017.
 */
public class ValidationErrorDto {

    private String field;
    private String message;

    public ValidationErrorDto(String field, String message) {
        this.field = field;
        this.message = message;
    }

    public String getField() {
        return field;
    }

    public String getMessage() {
        return message;
    }
}
