package ro.johann.dto;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * Created by Johann on 5/25/2017.
 */
public class NGramInputDto {

    @NotNull
    @Min(1)
    private Integer maxNGramCount;

    @NotNull
    @NotEmpty
    private String text;

    public NGramInputDto() {
    }

    public NGramInputDto(Integer maxNGramCount, String text) {
        this.maxNGramCount = maxNGramCount;
        this.text = text;
    }

    public Integer getMaxNGramCount() {
        return maxNGramCount;
    }

    public void setMaxNGramCount(Integer maxNGramCount) {
        this.maxNGramCount = maxNGramCount;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
