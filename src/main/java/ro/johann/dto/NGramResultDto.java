package ro.johann.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Created by Johann on 5/25/2017.
 */
public class NGramResultDto {

    private String nGram;
    private int length;
    private int count;

    public NGramResultDto(String nGram, int length, int count) {
        this.nGram = nGram;
        this.length = length;
        this.count = count;
    }

    public String getNGram() {
        return nGram;
    }

    @JsonIgnore
    public int getLength() {
        return length;
    }

    public int getCount() {
        return count;
    }
}
