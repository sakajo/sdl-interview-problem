package ro.johann.service.ngram;

import java.util.HashMap;

/**
 * Created by Johann on 5/26/2017.
 */
public class NGramNode {

    private HashMap<String, NGramNode> children = new HashMap<>();
    private int score = 0;

    public void incrementScore() {
        score++;
    }

    public HashMap<String, NGramNode> getChildren() {
        return children;
    }

    public int getScore() {
        return score;
    }

    public void addChild(String key) {
        children.put(key, new NGramNode());
    }

}
