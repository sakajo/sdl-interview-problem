package ro.johann.service.ngram;

import org.springframework.stereotype.Component;
import ro.johann.dto.NGramResultDto;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

/**
 * Created by Johann on 5/26/2017.
 */
@Component
public class NGramResultReader implements INGramResultReader {

    private TreeSet<NGramResultDto> nGramTreeResult;

    public List<NGramResultDto> readResult(NGramNode nGramForest) {

        if (nGramForest == null)
            throw new InvalidParameterException();
        nGramTreeResult = new TreeSet<>((left, right) -> {
            if (left.getLength() != right.getLength())
                return Integer.compare(right.getLength(), left.getLength());
            else if (left.getCount() != right.getCount())
                return Integer.compare(right.getCount(), left.getCount());
            else
                return left.getNGram().compareTo(right.getNGram());
        });

        readResultRecursively(nGramForest, "", 1);

        return new ArrayList<>(nGramTreeResult);
    }

    private void readResultRecursively(NGramNode root, String currentNGram, int depth) {

        for (Map.Entry<String, NGramNode> nGramEntry : root.getChildren().entrySet()) {

            String nGram = currentNGram + nGramEntry.getKey();
            NGramResultDto newNGram = new NGramResultDto(currentNGram + nGramEntry.getKey(), depth, nGramEntry.getValue().getScore());
            nGramTreeResult.add(newNGram);
            readResultRecursively(nGramEntry.getValue(), nGram + " ", depth + 1);
        }
    }
}
