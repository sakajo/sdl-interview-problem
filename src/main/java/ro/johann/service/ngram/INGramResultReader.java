package ro.johann.service.ngram;

import ro.johann.dto.NGramResultDto;

import java.util.List;

/**
 * Created by Johann on 5/26/2017.
 */
public interface INGramResultReader {

    List<NGramResultDto> readResult(NGramNode nGramForest);
}
