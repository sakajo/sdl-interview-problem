package ro.johann.service.ngram;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.johann.dto.NGramInputDto;
import ro.johann.dto.NGramResultDto;

import java.util.List;

/**
 * Created by Johann on 5/25/2017.
 */
@Service
public class NGramService implements INGramService {

    @Autowired
    private INGramResultReader resultReader;

    @Autowired
    private INGramForestCalculator forestCalculator;

    @Override
    public List<NGramResultDto> countNGrams(NGramInputDto nGramInput) {

        NGramNode forest = forestCalculator.calculate(nGramInput.getText(), nGramInput.getMaxNGramCount());
        List<NGramResultDto> result = resultReader.readResult(forest);
        return result;
    }

    public void setResultReader(INGramResultReader resultReader) {
        this.resultReader = resultReader;
    }

    public void setForestCalculator(INGramForestCalculator forestCalculator) {
        this.forestCalculator = forestCalculator;
    }
}
