package ro.johann.service.ngram;

import ro.johann.dto.NGramInputDto;
import ro.johann.dto.NGramResultDto;

import java.util.List;

/**
 * Created by Johann on 5/25/2017.
 */
public interface INGramService {

    List<NGramResultDto> countNGrams(NGramInputDto nGramInput);
}
