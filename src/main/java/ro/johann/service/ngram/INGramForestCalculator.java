package ro.johann.service.ngram;

/**
 * Created by Johann on 5/26/2017.
 */
public interface INGramForestCalculator {

    NGramNode calculate(String text, int maxNGramCount);
}
