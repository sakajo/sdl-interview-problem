package ro.johann.service.ngram;

import org.springframework.stereotype.Component;

import java.security.InvalidParameterException;

/**
 * Created by Johann on 5/26/2017.
 */
@Component
public class NGramForestCalculator implements INGramForestCalculator {

    @Override
    public NGramNode calculate(String text, int maxNGramCount) {

        if (text == null || text.isEmpty() || maxNGramCount <= 0)
            throw new InvalidParameterException("Bad Arguments");

        NGramNode root = new NGramNode();

        String[] words = text.split(" ");
        for (int index = 0; index < words.length; index++) {

            NGramNode node = root;
            for (int depth = 0; depth < maxNGramCount && index + depth < words.length; depth++) {

                String word = words[index + depth];
                NGramNode nextNode = node.getChildren().get(word);
                if (nextNode == null) {
                    node.addChild(word);
                    nextNode = node.getChildren().get(word);
                }
                nextNode.incrementScore();
                node = nextNode;
            }
        }
        return root;
    }
}
