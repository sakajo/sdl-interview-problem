package ro.johann.service.format;

import org.springframework.stereotype.Component;

import java.security.InvalidParameterException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Johann on 5/25/2017.
 */
@Component
public class PhrasePairsConverter implements IPhrasePairsConverter {

    @Override
    public List<PhrasePair> convert(String text) throws InvalidParameterException {

        String[] words = text.split(" ");
        List<PhrasePair> phrasePairs = new LinkedList<>();
        try {
            int index = 0;
            while (index < words.length) {
                checkMatch("PP", words[index++]);
                int sourceTokensCount = Integer.parseInt(words[index++]);
                int targetTokensCount = Integer.parseInt(words[index++]);
                List<String> sourceTokens = getTokenList(words, index, sourceTokensCount);
                index += sourceTokensCount;
                checkMatch("$", words[index++]);
                List<String> targetTokens = getTokenList(words, index, targetTokensCount);
                index += targetTokensCount;
                checkMatch("&", words[index++]);
                // maybe I should reconsider this. can otherInfo have 0 or more than 1 words??
                String otherInfo = words[index++];
                phrasePairs.add(new PhrasePair(sourceTokens, targetTokens, otherInfo));
            }
        }
        catch (ArrayIndexOutOfBoundsException ex) {
            throw new InvalidParameterException("input text format is not valid");
        }
        return phrasePairs;
    }

    private void checkMatch(String expected, String outcome) {

        if (!expected.equals(outcome))
            throw new InvalidParameterException("input text format is not valid");
    }

    private List<String> getTokenList(String[] words, int cursor, int count) {

        List<String> tokens = new LinkedList<>();
        for (int index = 0; index < count; index++) {
            tokens.add(words[cursor + index]);
        }
        return tokens;
    }
}
