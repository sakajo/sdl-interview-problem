package ro.johann.service.format;

import java.util.List;

/**
 * Created by Johann on 5/25/2017.
 */
public interface IPhrasePairsFormatter {

    String format(List<PhrasePair> phrasePairs);
}
