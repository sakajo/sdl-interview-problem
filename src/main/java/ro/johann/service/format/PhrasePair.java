package ro.johann.service.format;

import java.util.List;

/**
 * Created by Johann on 5/25/2017.
 */
public class PhrasePair {

    private List<String> sourceTokens;
    private List<String> targetTokens;
    private String otherInfo;

    public PhrasePair() {
    }

    public PhrasePair(List<String> sourceTokens, List<String> targetTokens, String otherInfo) {
        this.sourceTokens = sourceTokens;
        this.targetTokens = targetTokens;
        this.otherInfo = otherInfo;
    }

    public List<String> getSourceTokens() {
        return sourceTokens;
    }

    public List<String> getTargetTokens() {
        return targetTokens;
    }

    public String getOtherInfo() {
        return otherInfo;
    }
}
