package ro.johann.service.format;

import ro.johann.dto.TextDto;

/**
 * Created by Johann on 5/25/2017.
 */
public interface IFormatService {

    TextDto format(TextDto rawFormat);
}
