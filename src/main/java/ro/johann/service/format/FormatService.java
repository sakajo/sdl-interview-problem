package ro.johann.service.format;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.johann.dto.TextDto;

import java.util.List;

/**
 * Created by Johann on 5/25/2017.
 */
@Service
public class FormatService implements IFormatService {

    @Autowired
    IPhrasePairsConverter phrasePairsConverter;

    @Autowired
    IPhrasePairsFormatter phrasePairsFormatter;

    @Override
    public TextDto format(TextDto textDto) {

        List<PhrasePair> phrasePairs = phrasePairsConverter.convert(textDto.getText());
        String formattedText = phrasePairsFormatter.format((phrasePairs));

        return new TextDto(formattedText);
    }

    public void setPhrasePairsConverter(IPhrasePairsConverter phrasePairsConverter) {
        this.phrasePairsConverter = phrasePairsConverter;
    }

    public void setPhrasePairsFormatter(IPhrasePairsFormatter phrasePairsFormatter) {
        this.phrasePairsFormatter = phrasePairsFormatter;
    }
}
