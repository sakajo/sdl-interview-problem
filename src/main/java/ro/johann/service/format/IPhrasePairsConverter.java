package ro.johann.service.format;

import java.security.InvalidParameterException;
import java.util.List;

/**
 * Created by Johann on 5/25/2017.
 */
public interface IPhrasePairsConverter {

    List<PhrasePair> convert(String text) throws InvalidParameterException;
}
