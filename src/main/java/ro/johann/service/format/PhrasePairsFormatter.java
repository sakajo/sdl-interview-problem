package ro.johann.service.format;

import org.springframework.stereotype.Component;

import java.security.InvalidParameterException;
import java.util.List;
import java.util.StringJoiner;

/**
 * Created by Johann on 5/25/2017.
 */
@Component
public class PhrasePairsFormatter implements IPhrasePairsFormatter {

    @Override
    public String format(List<PhrasePair> phrasePairs) {

        if (phrasePairs == null || phrasePairs.size() == 0)
            throw new InvalidParameterException("The list is null or empty");
        StringJoiner stringJoiner = new StringJoiner(" ");

        for (PhrasePair phrasePair : phrasePairs) {
            stringJoiner.add(String.valueOf(phrasePair.getSourceTokens().size()));
            for (String sourceToken : phrasePair.getSourceTokens())
                stringJoiner.add(sourceToken);
            stringJoiner.add("$");
            stringJoiner.add(String.valueOf(phrasePair.getTargetTokens().size()));
            for (String targetToken : phrasePair.getTargetTokens())
                stringJoiner.add(targetToken);
            stringJoiner.add("&");
            stringJoiner.add(phrasePair.getOtherInfo());
            stringJoiner.add("|");
        }
        return stringJoiner.toString();
    }
}
